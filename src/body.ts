export interface DataBody {
    /**
     * Remove the data from database
     */
    remove()
}

export interface FlatDataBody extends DataBody {
    /**
     * Get the type of the data to add support for multiple types
     * @return a type of the object
     */
    getSerializedType(): String;
}

export interface MultiTypeBody extends FlatDataBody, SqlDataBody {

}

export interface SqlDataBody extends DataBody {
    /**
     * Name of the table
     *
     * @return name of the table
     */
    getTable(): String;

    /**
     * Primary key of the object
     *
     * @return serialized primary key
     */
    getKey(): String;

    /**
     * Structure of the table
     * First value of the array is primary key!
     *
     * @return an array of strings for table structure
     */
    getStructure(): String[];
}

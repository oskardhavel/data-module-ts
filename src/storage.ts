import {DataBody} from "./body";
import Stream from "ts-stream";
export abstract class StorageHolder {

}

export abstract class Storage<T extends DataBody> implements Iterable<T> {

    holder: StorageHolder

    constructor(holder: StorageHolder) {
        this.holder = holder;
    }

    protected abstract onAdd(object: T)

    protected abstract onRemove(object: T)

    abstract [Symbol.iterator](): Iterator<T>

    add(object: T) {
        this.onAdd(object)
        this.save(object, true)
    }

    remove(object: T) {
        this.onRemove(object)
    }

    abstract load(async?: boolean, callback?: Function);

    abstract save(async?: boolean, callback?: Function);

    abstract save(object: T, async?: boolean, callback?: Function)

    abstract stream(): Stream<T>
}

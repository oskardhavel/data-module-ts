export interface Pair<T1, T2> {
    key?: T1
    value?: T2
}

export class Preconditions {
    static checkArgument(argument: boolean, error?: string) {
        if (!argument)
            throw new Error(error == null ? "Failed to Check Argument" : error)
    }
}

export class SerializedData {
    object: any
    constructor(object?: any) {
        if (!object)
            this.object = new Map<any, any>()
        else
            this.object = object
    }

    applyAs<T>(): T {
        Preconditions.checkArgument(this.object instanceof Map, "This function only works for objects, not maps")
        return this.object as T
    }

    applyAsCollection(): SerializedData[] {
        Preconditions.checkArgument(this.object !instanceof Map, "This function only works for objects, not maps")
        return (this.object as any[]).map(ob => new SerializedData(ob))
    }

    applyAsMap(): Pair<SerializedData, SerializedData>[] {
        Preconditions.checkArgument(this.object !instanceof Map, "This function only works for objects, not maps")
        return Array
            .from((this.object as Map<any, any>))
            .map(([k, v]) => <Pair<SerializedData, SerializedData>> {key: k, value: v})
    }

    children(field: string): SerializedData {
        Preconditions.checkArgument(this.object instanceof Map, "This function only works for maps, not objects!")
        return (this.object as Map<string, SerializedData>)[field]
    }

    write(field: string, object: any) {
        Preconditions.checkArgument(this.object instanceof Map, "This function only works for maps, not objects!");
        (this.object as Map<string, SerializedData>).set(field, new SerializedData(object))
    }
}